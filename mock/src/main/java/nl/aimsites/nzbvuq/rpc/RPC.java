package nl.aimsites.nzbvuq.rpc;

import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.concurrent.CompletableFuture;

/**
 * The interface Rpc.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public interface RPC {
  /**
   * Send request completable future.
   *
   * @param destination Destination key to send the message to
   * @param request Request to send to the destination
   * @return returns a CompletableFuture that returns once a Response has been received
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  CompletableFuture<Response> sendRequest(String destination, Request request);

  /**
   * Get identifier
   *
   * @return String defining the identifier
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  String getIdentifier();
}
