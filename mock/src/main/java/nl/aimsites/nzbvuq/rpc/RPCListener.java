package nl.aimsites.nzbvuq.rpc;

import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class RPCListener implements RPC {

  private final String identifier;

  public RPCListener() {
    this(UUID.randomUUID().toString());
  }

  public RPCListener(String identifier) {
    this.identifier = identifier;
  }

  @Override
  public CompletableFuture<Response> sendRequest(String destination, Request request) {
    return CompletableFuture.supplyAsync(
        () -> {
          switch (request.getMethod()) {
            case "methodName":
              // Instead, you would call an interface here, instead of defining the response
              // yourself
              ArrayList<String> arguments = new ArrayList<>();
              arguments.add("responseArgument1");
              arguments.add("responseArgument2");
              return new Response(request.getId(), arguments);
            default:
              return new Response(request.getId(), new ArrayList<>());
          }
        });
  }

  @Override
  public String getIdentifier() {
    return identifier;
  }
}
