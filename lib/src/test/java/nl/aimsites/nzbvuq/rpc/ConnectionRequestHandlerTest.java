package nl.aimsites.nzbvuq.rpc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ConnectionRequestHandlerTest {

  static HandlerTestClass handlerTestClass;
  List<String> suppliedList;
  List<String> expectedList;

  @BeforeAll
  static void beforeAll() {
    handlerTestClass = mock(HandlerTestClass.class);
    ConnectionRequestHandler.getInstance().addHandler(handlerTestClass);
  }

  @BeforeEach
  void setUp() {
    suppliedList = new ArrayList<>();
    suppliedList.add("RequestArgument1");
    suppliedList.add("RequestArgument2");

    expectedList = new ArrayList<>();
    expectedList.add("ResponseArgument1");
    expectedList.add("ResponseArgument2");

    when(handlerTestClass.testMethod(any())).thenReturn(expectedList);
  }

  @Test
  void executeMethod() throws Exception {
    List<String> result;

    result =
        ConnectionRequestHandler.getInstance()
            .executeMethod(handlerTestClass.getClass().getName(), "testMethod", suppliedList);

    ArgumentCaptor<List<String>> listArgumentCaptor = ArgumentCaptor.forClass(List.class);
    verify(handlerTestClass, times(1)).testMethod(listArgumentCaptor.capture());
    assertEquals(suppliedList, listArgumentCaptor.getValue());
    assertEquals(expectedList, result);
  }

  @Test
  void executeNoSuchMethod() {
    assertThrows(
        NoSuchMethodException.class,
        () -> {
          ConnectionRequestHandler.getInstance()
              .executeMethod(handlerTestClass.getClass().getName(), "noSuchMethod", suppliedList);
        });
  }

  @Test
  void executeNoSuchClass() throws Exception {
    List<String> result;

    result =
        ConnectionRequestHandler.getInstance()
            .executeMethod("noSuchClass", "testMethod", suppliedList);

    assertArrayEquals(new ArrayList<>().toArray(), result.toArray());
  }

  public static class HandlerTestClass {
    public List<String> testMethod(List<String> arguments) {
      return arguments;
    }
  }
}
