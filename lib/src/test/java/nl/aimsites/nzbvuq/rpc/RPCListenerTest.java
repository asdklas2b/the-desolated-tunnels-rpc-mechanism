package nl.aimsites.nzbvuq.rpc;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.networking.contracts.ConnectionContract;
import nl.aimsites.nzbvuq.networking.contracts.MessageContract;
import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import nl.aimsites.nzbvuq.rpc.message.ExceptionResponse;
import nl.aimsites.nzbvuq.rpc.message.RPCPayloadType;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RPCListenerTest {

  private static final Gson GSON = new Gson();
  private static ConnectionRequestHandlerTest.HandlerTestClass handlerTestClass;
  private ConnectionContract connectionContract;
  private RPCListener rpcListener;
  private Request request;
  private List<String> requestParameters;

  @BeforeAll
  static void beforeAll() {
    handlerTestClass = new ConnectionRequestHandlerTest.HandlerTestClass();
    ConnectionRequestHandler.getInstance().addHandler(handlerTestClass);
  }

  @BeforeEach
  void setUp() throws ConnectionException {
    connectionContract = mock(ConnectionContract.class);

    doReturn(true).when(connectionContract).isInitialized();

    rpcListener = new RPCListener(connectionContract);

    requestParameters = new ArrayList<>();
    requestParameters.add("RequestArgument1");
    requestParameters.add("RequestArgument2");
    request = new Request(handlerTestClass.getClass().getName(), "testMethod", requestParameters);
  }

  @Test
  void sendRequest() throws Exception {
    String destination = "destination";

    List<String> responseParameters = new ArrayList<>();
    responseParameters.add("ResponseArgument1");
    responseParameters.add("ResponseArgument2");

    Response inputResponse = new Response(request.getId(), responseParameters);
    MessageContract expectedMessage = new RPCMessage("source", GSON.toJson(inputResponse));

    doAnswer(
            a -> {
              rpcListener.onMessage(expectedMessage);
              return null;
            })
        .when(connectionContract)
        .send(anyString(), any(MessageContract.class));
    when(connectionContract.isInitialized()).thenReturn(false).thenReturn(true);

    CompletableFuture<Response> response = rpcListener.sendRequest(destination, request);

    Response actualResponse = response.get();

    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<MessageContract> messageContractArgumentCaptor =
        ArgumentCaptor.forClass(MessageContract.class);

    verify(connectionContract, times(1))
        .send(stringArgumentCaptor.capture(), messageContractArgumentCaptor.capture());

    MessageContract messageContract = messageContractArgumentCaptor.getValue();

    assertEquals(request.getId(), actualResponse.getId());
    assertEquals(RPCPayloadType.RESPONSE, actualResponse.getType());
    assertArrayEquals(responseParameters.toArray(), actualResponse.getParameters().toArray());
    assertEquals(destination, stringArgumentCaptor.getValue());
    assertEquals(rpcListener.getIdentifier(), messageContract.getSource());
    assertEquals(GSON.toJson(request), messageContract.getPayload());
  }

//  @Test
//  void sendRequestExceptionOnSend() throws Exception {
//    String destination = "destination";
//
//    ConnectionException connectionException = new ConnectionException("ConnectionException");
//
//    doThrow(connectionException)
//        .when(connectionContract)
//        .send(anyString(), any(MessageContract.class));
//    when(connectionContract.isInitialized()).thenReturn(true);
//
//    CompletableFuture<Response> response = rpcListener.sendRequest(destination, request);
//
//    Response actualResponse = response.get();
//
//    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
//    ArgumentCaptor<MessageContract> messageContractArgumentCaptor =
//        ArgumentCaptor.forClass(MessageContract.class);
//
//    verify(connectionContract, times(1))
//        .send(stringArgumentCaptor.capture(), messageContractArgumentCaptor.capture());
//
//    MessageContract messageContract = messageContractArgumentCaptor.getValue();
//
//    assertEquals(request.getId(), actualResponse.getId());
//    assertEquals(RPCPayloadType.EXCEPTION, actualResponse.getType());
//    assertTrue(actualResponse instanceof ExceptionResponse);
//    assertEquals(
//        connectionException, ((ExceptionResponse) actualResponse).getRpcException().getCause());
//    assertEquals(destination, stringArgumentCaptor.getValue());
//    assertEquals(rpcListener.getIdentifier(), messageContract.getSource());
//    assertEquals(GSON.toJson(request), messageContract.getPayload());
//  }

  /**
   * Note: This test will always take 5 seconds because this is the set timeout for receiving
   * requests
   */
  @Test
  void sendRequestTimeoutException() throws Exception {
    String destination = "destination";

    CompletableFuture<Response> response = rpcListener.sendRequest(destination, request);

    Response actualResponse = response.get();

    when(connectionContract.isInitialized()).thenReturn(true);

    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<MessageContract> messageContractArgumentCaptor =
        ArgumentCaptor.forClass(MessageContract.class);

    verify(connectionContract, times(1))
        .send(stringArgumentCaptor.capture(), messageContractArgumentCaptor.capture());

    MessageContract messageContract = messageContractArgumentCaptor.getValue();

    assertEquals(request.getId(), actualResponse.getId());
    assertEquals(RPCPayloadType.EXCEPTION, actualResponse.getType());
    assertEquals(
        "Response timed out", ((ExceptionResponse) actualResponse).getRpcException().getMessage());
    assertEquals(destination, stringArgumentCaptor.getValue());
    assertEquals(rpcListener.getIdentifier(), messageContract.getSource());
    assertEquals(GSON.toJson(request), messageContract.getPayload());
  }

  @Test
  void onMessage() throws Exception {
    MessageContract messageContract = new RPCMessage("source", GSON.toJson(request));

    rpcListener.onMessage(messageContract);

    ArgumentCaptor<RPCMessage> messageContractMockArgumentCaptor =
        ArgumentCaptor.forClass(RPCMessage.class);

    verify(connectionContract, times(1))
        .send(eq(messageContract.getSource()), messageContractMockArgumentCaptor.capture());

    RPCMessage rpcMessage = messageContractMockArgumentCaptor.getValue();
    assertEquals(rpcListener.getIdentifier(), rpcMessage.getSource());
    Response response = GSON.fromJson(rpcMessage.getPayload(), Response.class);
    assertEquals(request.getId(), response.getId());
    assertArrayEquals(requestParameters.toArray(), response.getParameters().toArray());
  }

  @Test
  void runStopsOnException() throws Exception {
    doThrow(new ConnectionException("ConnectionException")).when(connectionContract).listen();

    rpcListener.run();

    assertTrue(Thread.currentThread().isInterrupted());
  }
}
