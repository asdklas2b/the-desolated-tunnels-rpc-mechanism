package nl.aimsites.nzbvuq.networking.io.protocol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SixtyNineScannerTest {

  private final String payload =
      "{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\",\"payload\":\"This is some message\"}";
  private SixtyNineScanner sut;

  @BeforeEach
  void setUp() {
    var buffer = ByteBuffer.allocate(payload.length() + 4);

    buffer.order(ByteOrder.BIG_ENDIAN);
    buffer.putInt(payload.length());
    buffer.put(payload.getBytes(StandardCharsets.UTF_8));

    sut = new SixtyNineScanner(new ByteArrayInputStream(buffer.array()));
  }

  @Test
  void next() throws IOException {
    var packet = sut.next();

    assertArrayEquals(payload.getBytes(StandardCharsets.UTF_8), packet);
  }
}
