package nl.aimsites.nzbvuq.networking.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageSeralizerTest {

  private final String message =
      "{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\",\"payload\":\"VGhpcyBpcyBzb21lIG1lc3NhZ2U\\u003d\"}";
  private MessageSeralizer sut;

  @BeforeEach
  void setUp() {
    sut = MessageSeralizer.getInstance();
  }

  @Test
  void decode() {
    var result = sut.decode(message);

    assertEquals("This is some message", result.getPayload());
    assertEquals("here", result.getSource());
  }

  @Test
  void encode() {
    var payload =
        sut.encode(
            new Message("here", MessagePayloadType.MESSAGE, "This is some message", "somewhere"));

    assertEquals(message, payload);
  }

  @Test
  void encodeWithNullWillNotIncludeField() {
    var payload =
      sut.encode(
        new Message("here", MessagePayloadType.MESSAGE, null, "somewhere"));

    assertEquals("{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\"}", payload);
  }
}
