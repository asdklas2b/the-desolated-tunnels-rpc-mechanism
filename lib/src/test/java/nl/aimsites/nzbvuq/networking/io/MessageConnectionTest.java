package nl.aimsites.nzbvuq.networking.io;

import nl.aimsites.nzbvuq.networking.io.protocol.SixtyNineConnection;
import nl.aimsites.nzbvuq.networking.io.protocol.SixtyNineConnectionListener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class MessageConnectionTest {
  private final String inputMessage =
      "{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\",\"payload\":\"VGhpcyBpcyBzb21lIG1lc3NhZ2U\\u003d\"}";
  private MessageConnection sut;
  private SixtyNineConnection protocol;

  @BeforeEach
  void setUp() {
    protocol = Mockito.mock(SixtyNineConnection.class);

    sut = new MessageConnection(protocol);
  }

  @Test
  void send() throws IOException {
    var message =
        new Message("here", MessagePayloadType.MESSAGE, "This is some message", "somewhere");

    sut.send(message);

    verify(protocol).send(inputMessage);
  }

  @Test
  void receive() throws IOException {
    Mockito.doReturn(inputMessage).when(protocol).next();

    var out = (Message) sut.receive();

    assertEquals("This is some message", out.getPayload());
    assertEquals("here", out.getSource());
    assertEquals("somewhere", out.getDestination());
    assertEquals(MessagePayloadType.MESSAGE, out.getPayloadType());
  }

  @Test
  void listen() throws IOException {
    ArgumentCaptor<SixtyNineConnectionListener> sixtyNineListener =
        ArgumentCaptor.forClass(SixtyNineConnectionListener.class);

    var listener = Mockito.mock(MessageConnectionListener.class);

    sut.listen(listener);

    verify(protocol).listen(sixtyNineListener.capture());

    sixtyNineListener.getValue().onPacket(inputMessage);

    ArgumentCaptor<Message> message = ArgumentCaptor.forClass(Message.class);

    verify(listener).onMessage(message.capture());

    var out = message.getValue();

    assertEquals("This is some message", out.getPayload());
    assertEquals("here", out.getSource());
    assertEquals("somewhere", out.getDestination());
    assertEquals(MessagePayloadType.MESSAGE, out.getPayloadType());
  }

  @Test
  void close() throws Exception {
    sut.close();

    verify(protocol, times(1)).close();
  }
}
