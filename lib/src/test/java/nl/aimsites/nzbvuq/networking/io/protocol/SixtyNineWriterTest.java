package nl.aimsites.nzbvuq.networking.io.protocol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SixtyNineWriterTest {

  private final String payload =
      "{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\",\"payload\":\"This is some message\"}";
  private SixtyNineWriter sut;
  private ByteArrayOutputStream output;

  @BeforeEach
  void setUp() {
    output = new ByteArrayOutputStream();

    sut = new SixtyNineWriter(output);
  }

  @Test
  void write() throws IOException {
    var expected = ByteBuffer.allocate(payload.length() + 4);
    expected.order(ByteOrder.BIG_ENDIAN);
    expected.putInt(payload.length());
    expected.put(payload.getBytes(StandardCharsets.UTF_8));

    sut.write(payload);

    assertArrayEquals(expected.array(), output.toByteArray());
  }
}
