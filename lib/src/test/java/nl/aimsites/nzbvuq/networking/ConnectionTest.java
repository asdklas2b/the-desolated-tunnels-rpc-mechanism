package nl.aimsites.nzbvuq.networking;

import nl.aimsites.nzbvuq.networking.contracts.ConnectionListenerContract;
import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import nl.aimsites.nzbvuq.networking.io.Message;
import nl.aimsites.nzbvuq.networking.io.MessageConnection;
import nl.aimsites.nzbvuq.networking.io.MessageConnectionListener;
import nl.aimsites.nzbvuq.networking.io.MessagePayloadType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ConnectionTest {

  private Connection sut;

  private MessageConnection messageConnection;

  private String identifier;

  private Message message =
      new Message("here", MessagePayloadType.MESSAGE, "This is some message", "somewhere");

  @BeforeEach
  void setUp() {
    identifier = UUID.randomUUID().toString();

    messageConnection = Mockito.mock(MessageConnection.class);

    sut = new Connection(messageConnection, identifier);
  }

  @Test
  void initializeWithoutError() throws ConnectionException, IOException {
    ArgumentCaptor<Message> message = ArgumentCaptor.forClass(Message.class);

    Mockito.doReturn(new Message(null, MessagePayloadType.INIT, null, identifier))
        .when(messageConnection)
        .receive();

    sut.initialize();

    Mockito.verify(messageConnection).send(message.capture());

    assertEquals(MessagePayloadType.INIT, message.getValue().getPayloadType());
    assertEquals(identifier, message.getValue().getSource());
    assertEquals(identifier, sut.getIdentifier());
    assertTrue(sut.isInitialized());
  }

  @Test
  void initializeWithMessageError() throws IOException {
    Mockito.doReturn(new Message(null, MessagePayloadType.ERROR, "Some random error", identifier))
        .when(messageConnection)
        .receive();

    var exception =
        assertThrows(
            ConnectionException.class,
            () -> {
              sut.initialize();
            });

    assertEquals("Unable to INIT connection `Some random error`", exception.getMessage());
    assertEquals(identifier, sut.getIdentifier());
    assertFalse(sut.isInitialized());
  }

  @Test
  void initializeWithException() throws IOException {
    var cause = new IOException("Some IOException");

    Mockito.doThrow(cause).when(messageConnection).receive();

    var exception =
        assertThrows(
            ConnectionException.class,
            () -> {
              sut.initialize();
            });

    assertEquals(cause, exception.getCause());
    assertFalse(sut.isInitialized());
  }

  @Test
  void listenWithoutException() throws ConnectionException, IOException {
    sut.setInitialized(true);

    ArgumentCaptor<MessageConnectionListener> messageListener =
        ArgumentCaptor.forClass(MessageConnectionListener.class);

    var listener = Mockito.mock(ConnectionListenerContract.class);

    sut.addListener(listener);

    sut.listen();

    Mockito.verify(messageConnection).listen(messageListener.capture());

    messageListener.getValue().onMessage(message);

    Mockito.verify(listener).onMessage(message);
  }

  @Test
  void listenWithException() throws IOException {
    sut.setInitialized(true);

    var messageListener = ArgumentCaptor.forClass(MessageConnectionListener.class);
    var cause = new IOException("Some IOException");

    Mockito.doThrow(cause).when(messageConnection).listen(messageListener.capture());

    var exception =
        assertThrows(
            ConnectionException.class,
            () -> {
              sut.listen();
            });

    assertEquals(cause, exception.getCause());
  }

  @Test
  void sendWithoutException() throws ConnectionException, IOException {
    sut.setInitialized(true);

    sut.send("somewhere", message);

    ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);

    Mockito.verify(messageConnection).send(messageCaptor.capture());

    var out = messageCaptor.getValue();

    assertEquals("This is some message", out.getPayload());
    assertEquals(sut.getIdentifier(), out.getSource());
    assertEquals("somewhere", out.getDestination());
    assertEquals(MessagePayloadType.MESSAGE, out.getPayloadType());
  }

  @Test
  void sendWithException() throws IOException {
    sut.setInitialized(true);

    var messageCaptor = ArgumentCaptor.forClass(Message.class);
    var cause = new IOException("Some IOException");

    Mockito.doThrow(cause).when(messageConnection).send(messageCaptor.capture());

    var exception =
        assertThrows(
            ConnectionException.class,
            () -> {
              sut.send("somewhere", message);
            });

    assertEquals(cause, exception.getCause());
  }
}
