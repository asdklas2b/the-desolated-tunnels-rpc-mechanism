package nl.aimsites.nzbvuq.networking.io.protocol;

import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SixtyNineConnectionIntegrationTest {

  private final String payload =
      "{\"source\":\"here\",\"destination\":\"somewhere\",\"payloadType\":\"MESSAGE\",\"payload\":\"This is some message\"}";
  private ByteArrayOutputStream output;

  private SixtyNineConnection sut;

  @BeforeEach
  void setUp() throws IOException, ConnectionException {
    Socket socket = Mockito.mock(Socket.class);

    var buffer = ByteBuffer.allocate(payload.length() + 4);

    buffer.order(ByteOrder.BIG_ENDIAN);
    buffer.putInt(payload.length());
    buffer.put(payload.getBytes(StandardCharsets.UTF_8));

    Mockito.doReturn(new ByteArrayInputStream(buffer.array())).when(socket).getInputStream();

    output = new ByteArrayOutputStream();

    Mockito.doReturn(output).when(socket).getOutputStream();

    sut = new SixtyNineConnection(socket);
  }

  @Test
  void send() throws IOException {
    var expected = ByteBuffer.allocate(payload.length() + 4);
    expected.order(ByteOrder.BIG_ENDIAN);
    expected.putInt(payload.length());
    expected.put(payload.getBytes(StandardCharsets.UTF_8));

    sut.send(payload);

    assertArrayEquals(expected.array(), output.toByteArray());
  }

  @Test
  void read() throws IOException {
    var packet = sut.next();

    assertEquals(payload, packet);
  }

  @Test
  void listen() throws IOException, InterruptedException {
    var listener = Mockito.mock(SixtyNineConnectionListener.class);

    new Thread(
            () -> {
              try {
                sut.listen(listener);
              } catch (IOException e) {
                e.printStackTrace();
              }
            })
        .start();

    Thread.sleep(200);

    Mockito.verify(listener, Mockito.never()).onPacket("");
    Mockito.verify(listener, Mockito.times(1)).onPacket(payload);
  }
}
