package nl.aimsites.nzbvuq.networking.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nl.aimsites.nzbvuq.networking.contracts.MessageContract;

/**
 * The type Message seralizer.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class MessageSeralizer {

  private static MessageSeralizer instance;

  private final Gson decoder = new Gson();

  /**
   * Gets instance.
   *
   * @return the instance
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public static MessageSeralizer getInstance() {
    if (instance == null) {
      instance = new MessageSeralizer();
    }
    return instance;
  }

  /**
   * Decode message contract.
   *
   * @param input the input
   * @return the message contract
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public MessageContract decode(String input) {
    return decoder.fromJson(input, Message.class);
  }

  /**
   * Encode string.
   *
   * @param message the message
   * @return the string
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public String encode(MessageContract message) {
    return decoder.toJson(message);
  }
}
