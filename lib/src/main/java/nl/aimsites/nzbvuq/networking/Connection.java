package nl.aimsites.nzbvuq.networking;

import nl.aimsites.nzbvuq.networking.contracts.ConnectionContract;
import nl.aimsites.nzbvuq.networking.contracts.ConnectionListenerContract;
import nl.aimsites.nzbvuq.networking.contracts.MessageContract;
import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import nl.aimsites.nzbvuq.networking.exceptions.ConnectionWriteException;
import nl.aimsites.nzbvuq.networking.io.Message;
import nl.aimsites.nzbvuq.networking.io.MessageConnection;
import nl.aimsites.nzbvuq.networking.io.MessagePayloadType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Connection.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class Connection implements ConnectionContract {
  private final List<ConnectionListenerContract> listeners;
  private final MessageConnection messageConnection;
  private boolean initialized;
  private String identifier;

  /**
   * Instantiates a new Connection.
   *
   * @param messageConnection the message connection
   * @param identifier the identifier
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public Connection(MessageConnection messageConnection, String identifier) {
    this.messageConnection = messageConnection;
    this.identifier = identifier;
    this.listeners = new ArrayList<>();
    this.initialized = false;
  }

  /**
   * Initialize.
   *
   * @throws ConnectionException the connection exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void initialize() throws ConnectionException {
    try {
      messageConnection.send(new Message(MessagePayloadType.INIT, identifier));

      var response = (Message) messageConnection.receive();

      if (response.getPayloadType() != MessagePayloadType.INIT) {
        throw new ConnectionException("Unable to INIT connection `" + response.getPayload() + "`");
      }

      setIdentifier(response.getDestination());
      setInitialized(true);
    } catch (IOException e) {
      throw new ConnectionException(e);
    }
  }

  @Override
  public void listen() throws ConnectionException {
    if (!isInitialized()) {
      initialize();
    }

    try {
      messageConnection.listen(
          message -> {
            if (((Message) message).getPayloadType() != MessagePayloadType.INIT) {
              for (var listener : listeners) {
                listener.onMessage(message);
              }
            }
          });
    } catch (IOException e) {
      throw new ConnectionException(e);
    }
  }

  @Override
  public void addListener(ConnectionListenerContract listener) {
    listeners.add(listener);
  }

  @Override
  public boolean isOpen() {
    return !messageConnection.isClosed();
  }

  @Override
  public void send(String destination, MessageContract message) throws ConnectionException {
    if (!isInitialized()) {
      throw new ConnectionException("Connection is not initialized");
    }

    try {
      messageConnection.send(
          new Message(identifier, MessagePayloadType.MESSAGE, message.getPayload(), destination));
    } catch (IOException e) {
      throw new ConnectionWriteException(e);
    }
  }

  @Override
  public void close() throws ConnectionException {
    try {
      messageConnection.close();
    } catch (IOException e) {
      throw new ConnectionException(e);
    }
  }

  @Override
  public boolean isInitialized() {
    return initialized;
  }

  /**
   * Sets initialized.
   *
   * @param initialized the initialized
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }

  @Override
  public String getIdentifier() {
    return identifier;
  }

  /**
   * Sets identifier.
   *
   * @param identifier the identifier
   * @throws ConnectionException the connection exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void setIdentifier(String identifier) throws ConnectionException {
    if (isInitialized()) {
      throw new ConnectionException("Cannot set identifier when a connection is initialized");
    }
    this.identifier = identifier;
  }
}
