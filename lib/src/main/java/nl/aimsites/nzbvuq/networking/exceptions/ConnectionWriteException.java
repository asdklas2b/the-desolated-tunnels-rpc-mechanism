package nl.aimsites.nzbvuq.networking.exceptions;

/**
 * The type Connection write exception.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class ConnectionWriteException extends ConnectionException {
  /**
   * Instantiates a new Connection write exception.
   *
   * @param e the e
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public ConnectionWriteException(Exception e) {
    super(e);
  }
}
