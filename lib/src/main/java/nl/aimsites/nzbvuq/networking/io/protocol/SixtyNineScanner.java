package nl.aimsites.nzbvuq.networking.io.protocol;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * The type Sixty nine scanner.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class SixtyNineScanner {
  private final InputStream stream;

  /**
   * Instantiates a new Sixty nine scanner.
   *
   * @param stream the stream
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public SixtyNineScanner(InputStream stream) {
    this.stream = stream;
  }

  /**
   * Next byte [ ].
   *
   * <p>Important: May not return empty packet
   *
   * @return the byte [ ]
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public byte[] next() throws IOException {
    var size = 0;

    // Todo: rewrite scanner implementation
    while (size == 0) {
      byte[] buffer = new byte[4];

      stream.read(buffer);

      size = ByteBuffer.wrap(buffer).order(ByteOrder.BIG_ENDIAN).getInt();
    }

    byte[] payload = new byte[size];

    stream.read(payload);

    return payload;
  }
}
