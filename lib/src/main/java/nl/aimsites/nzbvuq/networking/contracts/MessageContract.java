package nl.aimsites.nzbvuq.networking.contracts;

public interface MessageContract {

  /**
   * Get the source of the message
   *
   * @return returns the source of the message as a string
   */
  String getSource();

  /**
   * Get the payload of the message
   *
   * @return returns the payload of the message as a string
   */
  String getPayload();
}
