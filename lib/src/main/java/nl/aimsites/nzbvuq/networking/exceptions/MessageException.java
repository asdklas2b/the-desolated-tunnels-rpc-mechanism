package nl.aimsites.nzbvuq.networking.exceptions;

/**
 * The type Message exception.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class MessageException extends Exception {
  /**
   * Instantiates a new Message exception.
   *
   * @param e the e
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public MessageException(String e) {
    super(e);
  }
}
