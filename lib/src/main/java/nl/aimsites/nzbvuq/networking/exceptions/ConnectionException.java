package nl.aimsites.nzbvuq.networking.exceptions;

/**
 * The type Connection exception.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class ConnectionException extends Exception {
  /**
   * Instantiates a new Connection exception.
   *
   * @param e the e
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public ConnectionException(Exception e) {
    super(e);
  }

  /**
   * Instantiates a new Connection exception.
   *
   * @param e the e
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public ConnectionException(String e) {
    super(e);
  }
}
