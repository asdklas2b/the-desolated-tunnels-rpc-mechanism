package nl.aimsites.nzbvuq.networking.io;

import nl.aimsites.nzbvuq.networking.contracts.MessageContract;

import java.io.IOException;

/**
 * The interface Message connection listener.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public interface MessageConnectionListener {
  /**
   * Executes an action based on the received message
   *
   * @param message message the receiver receives.
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  void onMessage(MessageContract message) throws IOException;
}
