package nl.aimsites.nzbvuq.networking.io;

/**
 * The enum Message payload type.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public enum MessagePayloadType {
  CLOSE,
  INIT,
  ERROR,
  MESSAGE
}
