package nl.aimsites.nzbvuq.networking;

import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;

import java.io.IOException;
import java.net.Socket;

/**
 * The type Socket factory.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class SocketFactory {
  private static SocketFactory instance;

  private SocketFactory() {}

  /**
   * Gets instance.
   *
   * @return the instance
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public static SocketFactory getInstance() {
    if (instance == null) {
      instance = new SocketFactory();
    }
    return instance;
  }

  /**
   * Make socket.
   *
   * @param ip the ip
   * @param port the port
   * @return the socket
   * @throws ConnectionException the connection exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public Socket make(String ip, int port) throws ConnectionException {
    try {
      return new Socket(ip, port);
    } catch (IOException e) {
      throw new ConnectionException(e);
    }
  }
}
