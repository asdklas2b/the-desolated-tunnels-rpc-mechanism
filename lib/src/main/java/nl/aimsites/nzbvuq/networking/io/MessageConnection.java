package nl.aimsites.nzbvuq.networking.io;

import nl.aimsites.nzbvuq.networking.contracts.MessageContract;
import nl.aimsites.nzbvuq.networking.io.protocol.SixtyNineConnection;

import java.io.IOException;

/**
 * The type Message connection.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class MessageConnection {

  private final SixtyNineConnection connection;

  private MessageSeralizer serializer = MessageSeralizer.getInstance();

  /**
   * Instantiates a new Message connection.
   *
   * @param connection the connection
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public MessageConnection(SixtyNineConnection connection) {
    this.connection = connection;
  }

  /**
   * Send.
   *
   * @param message the message
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void send(Message message) throws IOException {
    connection.send(serializer.encode(message));
  }

  /**
   * Receive message contract.
   *
   * @return the message contract
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public MessageContract receive() throws IOException {
    return serializer.decode(connection.next());
  }

  /**
   * Listen.
   *
   * @param listener the listener
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void listen(MessageConnectionListener listener) throws IOException {
    connection.listen(packet -> listener.onMessage(serializer.decode(packet)));
  }

  /**
   * Is closed boolean.
   *
   * @return the boolean
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public boolean isClosed() {
    return connection.isClosed();
  }

  /**
   * Close.
   *
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void close() throws IOException {
    connection.close();
  }

  /**
   * Sets serializer.
   *
   * @param serializer the serializer
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void setSerializer(MessageSeralizer serializer) {
    this.serializer = serializer;
  }
}
