package nl.aimsites.nzbvuq.networking.contracts;

import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import nl.aimsites.nzbvuq.networking.exceptions.MessageException;

public interface ConnectionContract {

  /** Starts the listener on the socket. */
  void listen() throws ConnectionException;

  /**
   * Add a connection listener to list
   *
   * @param listener Listener that receives messages
   */
  void addListener(ConnectionListenerContract listener);

  /**
   * Check if the socket is open
   *
   * @return Returns true if the socket has opened correctly
   */
  boolean isOpen();

  /**
   * Send a message to another destination
   *
   * @param destination Destination key to send the message to
   * @param message Message to send to the reciever
   * @throws MessageException Exception whilst sending
   */
  void send(String destination, MessageContract message)
      throws MessageException, ConnectionException;

  /**
   * Get identifier
   *
   * @return String defining the identifier
   */
  String getIdentifier();

  /**
   * Connection is initialized
   *
   * @return true when connection is initialized
   */
  boolean isInitialized();

  /** Closes the socket */
  void close() throws ConnectionException;
}
