package nl.aimsites.nzbvuq.networking.io;

import nl.aimsites.nzbvuq.networking.contracts.MessageContract;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * The type Message.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class Message implements MessageContract {

  private final String source;
  private final String destination;
  private final MessagePayloadType payloadType;
  private final String payload;

  /**
   * Instantiates a new Message.
   *
   * @param source the source
   * @param payloadType the payload type
   * @param payload the payload
   * @param destination the destination
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public Message(
      String source, MessagePayloadType payloadType, String payload, String destination) {
    this.source = source;
    this.payloadType = payloadType;

    this.payload = payload == null ? null :
      new String(
        Base64.getEncoder()
          .encode(payload.getBytes(StandardCharsets.UTF_8)),
        StandardCharsets.UTF_8);
    this.destination = destination;
  }

  /**
   * Instantiates a new Message.
   *
   * @param payloadType the payload type
   * @param source the source
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public Message(MessagePayloadType payloadType, String source) {
    this(source, payloadType, null, null);
  }

  @Override
  public String getSource() {
    return source;
  }

  @Override
  public String getPayload() {
    return new String(
        Base64.getDecoder().decode(this.payload.replace("\\u003d", "=")), StandardCharsets.UTF_8);
  }

  /**
   * Gets destination.
   *
   * @return the destination
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Gets payload type.
   *
   * @return the payload type
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public MessagePayloadType getPayloadType() {
    return payloadType;
  }
}
