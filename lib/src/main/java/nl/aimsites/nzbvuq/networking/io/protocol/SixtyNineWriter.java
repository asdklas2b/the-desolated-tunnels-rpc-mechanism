package nl.aimsites.nzbvuq.networking.io.protocol;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

/**
 * The type Sixty nine writer.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class SixtyNineWriter {

  private final OutputStream stream;

  /**
   * Instantiates a new Sixty nine writer.
   *
   * @param stream the stream
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public SixtyNineWriter(OutputStream stream) {
    this.stream = stream;
  }

  /**
   * Write.
   *
   * @param payload the payload
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void write(String payload) throws IOException {
    var byteBuffer = ByteBuffer.allocate(payload.length() + 4);

    byteBuffer.order(ByteOrder.BIG_ENDIAN);
    byteBuffer.putInt(payload.length());
    byteBuffer.put(payload.getBytes(StandardCharsets.UTF_8));

    stream.write(byteBuffer.array());
    stream.flush();
  }
}
