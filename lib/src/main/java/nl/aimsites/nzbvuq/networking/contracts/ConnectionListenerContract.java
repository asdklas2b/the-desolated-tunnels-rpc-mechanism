package nl.aimsites.nzbvuq.networking.contracts;

public interface ConnectionListenerContract {

  /**
   * Executes an action based on the recieved message
   *
   * @param message message the reciever recieves.
   */
  void onMessage(MessageContract message);
}
