package nl.aimsites.nzbvuq.networking.io.protocol;

import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * The type Sixty nine connection.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class SixtyNineConnection {

  private final Socket socket;

  private final SixtyNineWriter writer;
  private final SixtyNineScanner scanner;

  /**
   * Instantiates a new Sixty nine connection.
   *
   * @param socket the socket
   * @throws ConnectionException the connection exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public SixtyNineConnection(Socket socket) throws ConnectionException {
    this.socket = socket;

    try {
      writer = new SixtyNineWriter(new BufferedOutputStream(socket.getOutputStream()));

      scanner = new SixtyNineScanner(new BufferedInputStream(socket.getInputStream()));
    } catch (IOException e) {
      throw new ConnectionException(e);
    }
  }

  /**
   * Send.
   *
   * @param payload the payload
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void send(String payload) throws IOException {
    writer.write(payload);
  }

  /**
   * Next string.
   *
   * @return the string
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public String next() throws IOException {
    return new String(scanner.next(), StandardCharsets.UTF_8);
  }

  /**
   * Listen.
   *
   * @param listener the listener
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void listen(SixtyNineConnectionListener listener) throws IOException {
    while (!socket.isClosed()) {
      listener.onPacket(next());
    }
  }

  /**
   * Is closed boolean.
   *
   * @return the boolean
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public boolean isClosed() {
    return socket.isClosed();
  }

  /**
   * Close.
   *
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void close() throws IOException {
    socket.close();
  }
}
