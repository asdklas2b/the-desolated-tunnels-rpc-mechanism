package nl.aimsites.nzbvuq.networking.io.protocol;

import java.io.IOException;

/**
 * The interface Sixty nine connection listener.
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public interface SixtyNineConnectionListener {
  /**
   * On packet.
   *
   * @param packet the packet
   * @throws IOException the io exception
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  void onPacket(String packet) throws IOException;
}
