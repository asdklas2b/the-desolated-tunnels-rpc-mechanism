package nl.aimsites.nzbvuq;

import nl.aimsites.nzbvuq.rpc.exceptions.RPCException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties reader to be used for reading out settings
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class PropertiesReader {
  private static final String PROPERTY_FILE_NAME = "config.properties";
  private static PropertiesReader instance;
  private final Properties properties;

  /**
   * Instantiates a new Properties reader.
   *
   * @throws IOException the io exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private PropertiesReader() throws IOException {
    this.properties = new Properties();
    try (InputStream inputStream =
        getClass().getClassLoader().getResourceAsStream(PropertiesReader.PROPERTY_FILE_NAME)) {
      if (inputStream != null) {
        properties.load(inputStream);
      }
    }
  }

  /**
   * Gets property by identifier.
   *
   * @param property the property
   * @return the property
   * @throws RPCException the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public static String getProperty(String property) throws RPCException {
    return PropertiesReader.getProperty(property, null);
  }

  /**
   * Gets property by identifier, with a default value to return..
   *
   * @param property the property
   * @param defaultValue the default value
   * @return the property
   * @throws RPCException the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public static String getProperty(String property, String defaultValue) throws RPCException {
    try {
      if (PropertiesReader.instance == null) {
        PropertiesReader.instance = new PropertiesReader();
      }
      return defaultValue == null
          ? PropertiesReader.instance.properties.getProperty(property)
          : PropertiesReader.instance.properties.getProperty(property, defaultValue);
    } catch (IOException e) {
      throw new RPCException(e);
    }
  }
}
