package nl.aimsites.nzbvuq;

import java.util.List;

/**
 * This is an example of a class with a callable method. You would add this as a handler to
 * ConnectionRequestHandler. The ConnectionRequestHandler will then call this class when it is
 * defined in a request.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class ClassWithSomeMethods {
  /**
   * An example method that can be called by RPC.
   *
   * @param parameters the request parameters
   * @return the response parameters
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public List<String> methodName(List<String> parameters) {
    return parameters;
  }
}
