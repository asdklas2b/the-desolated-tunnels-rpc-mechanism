package nl.aimsites.nzbvuq.rpc.message;

import java.util.ArrayList;
import java.util.List;

public class Response implements RPCPayload {
  private final String id;
  private final List<String> parameters;
  private final RPCPayloadType type;

  /**
   * Instantiates a new Response.
   *
   * @param id the id
   * @param parameters the parameters
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public Response(String id, List<String> parameters) {
    this.id = id;
    this.parameters = parameters;
    this.type = RPCPayloadType.RESPONSE;
  }

  /**
   * Instantiates a new Response.
   *
   * @param id the id
   * @param rpcPayloadType the rpc payload type
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  protected Response(String id, RPCPayloadType rpcPayloadType) {
    this.id = id;
    this.parameters = new ArrayList<>();
    this.type = rpcPayloadType;
  }

  public String getId() {
    return id;
  }

  public List<String> getParameters() {
    return parameters;
  }

  @Override
  public RPCPayloadType getType() {
    return type;
  }
}
