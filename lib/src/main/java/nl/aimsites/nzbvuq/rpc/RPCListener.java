package nl.aimsites.nzbvuq.rpc;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.PropertiesReader;
import nl.aimsites.nzbvuq.networking.Connection;
import nl.aimsites.nzbvuq.networking.SocketFactory;
import nl.aimsites.nzbvuq.networking.contracts.ConnectionContract;
import nl.aimsites.nzbvuq.networking.contracts.ConnectionListenerContract;
import nl.aimsites.nzbvuq.networking.contracts.MessageContract;
import nl.aimsites.nzbvuq.networking.exceptions.ConnectionException;
import nl.aimsites.nzbvuq.networking.exceptions.MessageException;
import nl.aimsites.nzbvuq.networking.io.MessageConnection;
import nl.aimsites.nzbvuq.networking.io.protocol.SixtyNineConnection;
import nl.aimsites.nzbvuq.rpc.exceptions.RPCException;
import nl.aimsites.nzbvuq.rpc.message.ExceptionResponse;
import nl.aimsites.nzbvuq.rpc.message.RPCPayload;
import nl.aimsites.nzbvuq.rpc.message.RPCPayloadType;
import nl.aimsites.nzbvuq.rpc.message.RPCPayloadTypeChecker;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Rpc listener.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class RPCListener implements RPC, ConnectionListenerContract, Runnable {
  /**
   * The constant LOGGER.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private static final Logger LOGGER = Logger.getLogger(RPCListener.class.getName());

  /**
   * The constant GSON.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private static final Gson GSON = new Gson();

  /**
   * The Connection contract.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private final ConnectionContract connectionContract;

  /**
   * The Received responses.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private final Map<String, CompletableFuture<Response>> futures;

  /**
   * Instantiates a new Rpc listener.
   *
   * @throws ConnectionException the connection exception
   * @throws RPCException the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCListener() throws ConnectionException, RPCException {
    this(UUID.randomUUID().toString());
  }

  /**
   * Instantiates a new Rpc listener.
   *
   * @param identifier the identifier
   * @throws ConnectionException the connection exception
   * @throws RPCException the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCListener(String identifier) throws ConnectionException, RPCException {
    this(
        new Connection(
            new MessageConnection(
                new SixtyNineConnection(
                    SocketFactory.getInstance()
                        .make(
                            PropertiesReader.getProperty("rpcIp"),
                            Integer.parseInt(PropertiesReader.getProperty("rpcPort"))))),
            identifier));
  }

  /**
   * Instantiates a new Rpc listener.
   *
   * @param connectionContract the connection contract
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCListener(ConnectionContract connectionContract) {
    this.connectionContract = connectionContract;
    this.connectionContract.addListener(this);
    new Thread(this).start();
    this.futures = new ConcurrentHashMap<>();
  }

  @Override
  public CompletableFuture<Response> sendRequest(String destination, Request request) {
    var future = new CompletableFuture<Response>();

    futures.put(request.getId(), future);

    while (true) {
      if (!connectionContract.isInitialized()) {
        try {
          Thread.sleep(0);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();

          completeFuture(new ExceptionResponse(request.getId(), new RPCException(e)));

          return future;
        }
      } else {
        break;
      }
    }

    MessageContract messageContract = new RPCMessage(this.getIdentifier(), GSON.toJson(request));
    try {
      connectionContract.send(destination, messageContract);
    } catch (ConnectionException | MessageException e) {
      completeFuture(new ExceptionResponse(request.getId(), new RPCException(e)));

      return future;
    }

    new Thread(
            () -> {
              try {
                Thread.sleep(500);
              } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
              }
              if (!future.isDone()) {
                completeFuture(
                    new ExceptionResponse(request.getId(), new RPCException("Response timed out")));
              }
            })
        .start();

    return future;
  }

  @Override
  public void onMessage(MessageContract message) {
    RPCPayload rpcPayload = GSON.fromJson(message.getPayload(), RPCPayloadTypeChecker.class);
    if (rpcPayload.getType() == RPCPayloadType.REQUEST) {
      Request request = GSON.fromJson(message.getPayload(), Request.class);
      Response response;
      try {
        response =
            new Response(
                request.getId(),
                ConnectionRequestHandler.getInstance()
                    .executeMethod(
                        request.getClassName(), request.getMethod(), request.getParameters()));
      } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
        LOGGER.log(Level.SEVERE, "RPC - Got exception whilst executing", e);

        ExceptionResponse exceptionResponse =
            new ExceptionResponse(request.getId(), new RPCException(e));
        try {
          this.connectionContract.send(
              message.getSource(),
              new RPCMessage(this.getIdentifier(), GSON.toJson(exceptionResponse)));
        } catch (ConnectionException | MessageException sendException) {
          LOGGER.log(Level.SEVERE, "Error occurred trying to send a response", sendException);
        }
        return;
      }
      try {
        this.connectionContract.send(
            message.getSource(), new RPCMessage(this.getIdentifier(), GSON.toJson(response)));
      } catch (ConnectionException | MessageException sendException) {
        LOGGER.log(Level.SEVERE, "Error occurred trying to send a response", sendException);
      }
    } else if (rpcPayload.getType() == RPCPayloadType.RESPONSE
        || rpcPayload.getType() == RPCPayloadType.EXCEPTION) {
      Response response = GSON.fromJson(message.getPayload(), Response.class);
      completeFuture(response);
    }
  }

  /**
   * Complete future from the list.
   *
   * @param response
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void completeFuture(Response response) {
    futures.get(response.getId()).complete(response);
  }

  public String getIdentifier() {
    return connectionContract.getIdentifier();
  }

  @Override
  public void run() {
    try {
      this.connectionContract.listen();
    } catch (ConnectionException e) {
      Thread.currentThread().interrupt();
    }
  }
}
