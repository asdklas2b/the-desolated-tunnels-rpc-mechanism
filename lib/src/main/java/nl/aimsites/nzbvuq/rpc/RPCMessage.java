package nl.aimsites.nzbvuq.rpc;

import nl.aimsites.nzbvuq.networking.contracts.MessageContract;

/**
 * The type Rpc message.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class RPCMessage implements MessageContract {
  private final String source;
  private final String payload;

  /**
   * Instantiates a new Rpc message.
   *
   * @param source the source
   * @param payload the payload
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCMessage(String source, String payload) {
    this.source = source;
    this.payload = payload;
  }

  @Override
  public String getSource() {
    return source;
  }

  @Override
  public String getPayload() {
    return payload;
  }
}
