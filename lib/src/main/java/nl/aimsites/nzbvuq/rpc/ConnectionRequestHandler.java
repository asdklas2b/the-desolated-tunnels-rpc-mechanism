package nl.aimsites.nzbvuq.rpc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The type Connection request handler.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class ConnectionRequestHandler {
  private static ConnectionRequestHandler instance;
  private final List<Object> objects = new ArrayList<>();

  /**
   * Instantiates a new Connection request handler.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private ConnectionRequestHandler() {}

  /**
   * Gets instance.
   *
   * @return the instance
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public static ConnectionRequestHandler getInstance() {
    if (instance == null) {
      ConnectionRequestHandler.instance = new ConnectionRequestHandler();
    }
    return instance;
  }

  /**
   * Add handler.
   *
   * @param handler the handler
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public void addHandler(Object handler) {
    Optional<Object> optionalObject = getObject(handler.getClass().getName());
    if (optionalObject.isEmpty()) {
      this.objects.add(handler);
    }
  }

  /**
   * Execute method list.
   *
   * @param className the class name
   * @param methodName the method name
   * @param arguments the arguments
   * @return the list
   * @throws NoSuchMethodException the no such method exception
   * @throws IllegalAccessException the illegal access exception
   * @throws InvocationTargetException the invocation target exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  @SuppressWarnings("unchecked")
  public List<String> executeMethod(String className, String methodName, List<String> arguments)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

    Optional<Object> optionalObject = getObject(className);
    if (optionalObject.isPresent()) {
      Object object = optionalObject.get();
      Method method = object.getClass().getMethod(methodName, List.class);
      return (List<String>) method.invoke(object, arguments);
    }
    return new ArrayList<>();
  }

  /**
   * Gets object.
   *
   * @param className the class name
   * @return the object
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  private Optional<Object> getObject(String className) {
    return this.objects.stream()
        .filter(object -> object.getClass().getName().equals(className))
        .findFirst();
  }
}
