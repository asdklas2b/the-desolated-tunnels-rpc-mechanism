package nl.aimsites.nzbvuq.rpc.exceptions;

/**
 * The type Rpc exception.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class RPCException extends Exception {
  /**
   * Instantiates a new Rpc exception.
   *
   * @param cause the cause
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCException(Throwable cause) {
    super(cause);
  }

  /**
   * Instantiates a new Rpc exception.
   *
   * @param message the message
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCException(String message) {
    super(message);
  }
}
