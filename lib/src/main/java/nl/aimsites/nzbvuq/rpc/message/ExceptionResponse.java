package nl.aimsites.nzbvuq.rpc.message;

import nl.aimsites.nzbvuq.rpc.exceptions.RPCException;

/**
 * The type Exception response.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class ExceptionResponse extends Response {
  private final RPCException rpcException;

  /**
   * Instantiates a new Exception response.
   *
   * @param id the id
   * @param rpcException the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public ExceptionResponse(String id, RPCException rpcException) {
    super(id, RPCPayloadType.EXCEPTION);
    this.rpcException = rpcException;
  }

  /**
   * Gets rpc exception.
   *
   * @return the rpc exception
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public RPCException getRpcException() {
    return rpcException;
  }
}
