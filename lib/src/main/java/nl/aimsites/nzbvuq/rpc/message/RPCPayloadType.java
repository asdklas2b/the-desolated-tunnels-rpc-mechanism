package nl.aimsites.nzbvuq.rpc.message;

/**
 * The enum Rpc payload type.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public enum RPCPayloadType {
  REQUEST,
  RESPONSE,
  EXCEPTION
}
