package nl.aimsites.nzbvuq.rpc.message;

/**
 * The type Rpc payload type checker.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class RPCPayloadTypeChecker implements RPCPayload {
  private RPCPayloadType type;

  @Override
  public RPCPayloadType getType() {
    return type;
  }
}
