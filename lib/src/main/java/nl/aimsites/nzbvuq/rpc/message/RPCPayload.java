package nl.aimsites.nzbvuq.rpc.message;

/**
 * The interface Rpc payload.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public interface RPCPayload {
  RPCPayloadType getType();
}
