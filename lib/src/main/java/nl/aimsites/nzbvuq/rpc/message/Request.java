package nl.aimsites.nzbvuq.rpc.message;

import java.util.List;
import java.util.UUID;

/**
 * The type Request.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
public class Request implements RPCPayload {
  private final String id;
  private final RPCPayloadType type;
  private final String className;
  private final String method;
  private final List<String> parameters;

  /**
   * Instantiates a new Request.
   *
   * @param className the class name
   * @param method the method
   * @param parameters the parameters
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public Request(String className, String method, List<String> parameters) {
    this.id = UUID.randomUUID().toString();
    this.className = className;
    this.method = method;
    this.parameters = parameters;
    this.type = RPCPayloadType.REQUEST;
  }

  public String getId() {
    return id;
  }

  public String getClassName() {
    return className;
  }

  public String getMethod() {
    return method;
  }

  public List<String> getParameters() {
    return parameters;
  }

  @Override
  public RPCPayloadType getType() {
    return type;
  }
}
